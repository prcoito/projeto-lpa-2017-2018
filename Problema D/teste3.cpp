#include <stack>

#define TRUE 1
#define FALSE 0 

#include <stdio.h>
#include <cstdlib>
#include <cstring>

using namespace std;
 
int cache[801];

int V;    // No. of vertices
int matrix[801][801];
int matrix_espelho[801][801];

int values[801];
int values_espelho[801];


int disc[801];
int low[801];
stack<int> *st;
int stackMember[801];
    
int root[801];    
    
int time = 1;


void SCCUtil(int u);
void SCC();

void SCCUtil(int u)
{
    // A static variable is used for simplicity, we can avoid use
    // of static variable by passing a pointer.
    
 
    // Initialize discovery time and low value
    disc[u] = low[u] = ++time;
    st->push(u);
    stackMember[u] = TRUE;
 
    // Go through all vertices adjacent to this
    for (int v=0; v<V; v++)
    {
        //v is current adjacent of 'u'
 		if(matrix[u][v]){
 
	        // If v is not visited yet, then recur for it
	        if (disc[v] == 0)
	        {
	            SCCUtil(v);
	 
	            // Check if the subtree rooted with 'v' has a
	            // connection to one of the ancestors of 'u'
	            low[u]  = min(low[u], low[v]);
	        }
	 
	        // Update low value of 'u' only of 'v' is still in stack
	        // (i.e. it's a back edge, not cross edge).
	        // Case 2 
	        else if (stackMember[v] == TRUE)
	            low[u]  = min(low[u], disc[v]);
    
		}
	
	}
 
    // head node found, pop the stack and print an SCC
    int w = 0;  // To store stack extracted vertices
    
    //printf("antes do if\n");
    if (low[u] == disc[u])
    {
    /*	
	for(int e=0; e<g->V; e++){
		printf("%d ", stackMember[e]);
    }
	printf("\n");
    */	
    	
        while (st->top() != u)
        {
            w = (int) st->top();
            
            values_espelho[u]+=values[w];
            
            //members[w]=1;
   			root[w]=u ;
			/*for (int edge = 0; edge < V; ++edge){
   			 	
   			 	
   			 	if( matrix[edge][w] && edge != u)
					matrix_espelho[edge][u]=1;
				
				if( matrix[w][edge] && edge != u)
   					matrix_espelho[u][edge]=1;
			
			}*/
            
            //printf("%d ",w);
			stackMember[w]=FALSE;
            st->pop();
        }
        
        w = (int) st->top();
        
        //members[w]=1;
        
   		values_espelho[w]+=values[w];

        //printf("%d\n",w);
		stackMember[w]=FALSE;
		
        st->pop();
		
		for(int w=0; w <V;++w){	
		 	if(root[w]==u){
         		for (int edge = 0; edge < V; ++edge){
   					
   					if( matrix[edge][w] && root[edge] != u) //!members[edge] &&
						matrix_espelho[root[edge]][u]=1;
			
					if( matrix[w][edge] && root[edge] != u)	//!members[edge] &&
   						matrix_espelho[u][root[edge]]=1;
    	 		}
    		}
    	}
    	//memset(members,0,sizeof(members));
    }
}
 
int magic_counter(int now){
	
	if (cache[now]){
		return cache [now];
	}
	
	int awesome=0;
	
	for (int ew = 0; ew < V; ++ew)
	{
		if(matrix_espelho[now][ew]){
			awesome = max(awesome, magic_counter(ew));
		}
	}
	cache[now] = awesome + values_espelho[now];
	return cache[now];
	
}
 
 
// The function to do DFS traversal. It uses SCCUtil()
void SCC()
{
	time=1;    
	memset(disc, 0, sizeof(disc) );
    memset(low, 0, sizeof(low) );
    memset(stackMember,0,sizeof(stackMember));
    
	memset(matrix_espelho, 0, sizeof(matrix_espelho));
    memset(values_espelho, 0, sizeof(values_espelho));
    
    st = new stack<int>();
    
    for(int i=0;i<V;++i){
    	
		root[i]=i;	
    	
    }
 
    // Call the recursive helper function to find strongly
    // connected components in DFS tree with vertex 'i'
    for (int i = 0; i < V; i++){
        if (disc[i] == 0){
        	//printf("SCCutil\n");
            SCCUtil(i);		
		}
	}
	//printf("acabei o tarzan\n");


	//###################################    MAGIC GOES HERE   ####################################
	/*
	for(int a=0; a< V; a++){
		
		printf("NO: i%d,n%d (PESO: %d)\n",a,a+1, values_espelho[a]);
		
    	for (int col = 0;col<V; ++col)
    	{
			if(matrix_espelho[a][col])
				printf("%d ",col);
		}
		printf("\n");

		for (int col = 0;col<V; ++col)
    	{
			if(matrix_espelho[col][a])
				printf("%d ",col);
		}
		printf("\n");
	}*/
	
	/*
	for (int l = 0; l < V; l++){
		if(values_espelho[l]==0){
			//printf("entrei nos pesos vazios %d\n",c);
			
    		for (int i = 0; i <V; ++i)
    		{
				matrix_espelho[i][l]=0;
				matrix_espelho[l][i]=0;
			}
		}	
	}
	
	
	for(int a=0; a< V; a++){
		
		printf("NO: %d (PESO: %d)\n",a, values_espelho[a]);
		
    	for (int col = 0;col<V; ++col)
    	{
			if(matrix_espelho[a][col])
				printf("%d ",col);
		}
		printf("\n");

		for (int col = 0;col<V; ++col)
    	{
			if(matrix_espelho[col][a])
				printf("%d ",col);
		}
		printf("\n");

	}*/
	
	memset(cache,0,sizeof(cache));
	
	int best=0;
	for (int c = 0; c < V; c++){
        if (values_espelho[c] !=0){
        	
			best= max(best, magic_counter(c));		
        }
            
	}
	printf("%d\n",best);
	
	
	//delete disc;
    //delete low;
    //free(stackMember);
    //delete st;
 	
 	//freeGraph(new_g);
}
 
// Driver program to test above function
int main()
{
	
	int n,r,i;
	int e1, e2;
	scanf("%d",&n);
	//printf("%d\n",n);
	while(n){
		memset(matrix, 0, sizeof(matrix));
		memset(values, 0, sizeof(values));
		
		scanf("%d %d", &V,&r );
		//printf("%d,%d\n",V,r);
		i=0;
		while(i<V){
			scanf("%d ", &(values[i]));
			//printf("%d ",values[i]);
			i++;
		}
		//printf("\n");
		
		while(r){
			scanf("%d %d", &e1,&e2 );
			//printf("%d,%d\n",e1,e2);
			matrix[e1-1][e2-1]=1;
			r--;
		}
		SCC();
		
		n--;
	}
    return 0;
}
