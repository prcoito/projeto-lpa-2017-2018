#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*variáveis globais*/
int k,n;

double x_points[130];
double y_points[130];

/*podia nao ser global, mas para ser mais f�cil a adapta�ao do merge sort...*/
double extra[130];

double bound[130];
double best=-0.100000000000;

/*declaracao de funcoes auxiliares*/
void maximizer(double area_now, int offset, int k_left, int last_point_picked);
int mergesort(double *input, int size);
double fabs(double x);

int main(){
	
	x_points[130]=0.000000000000;
	scanf("%d %d",&n,&k);
	int i=0;
	while(i<n){
		scanf("%lf %lf",&x_points[i],&y_points[i]);
		/*printf("%.12f\n",points[i]);*/
		i++;
	}
	mergesort(y_points, n);
	
	i=n-2;
	bound[n-1]=0.000000000000;
	while(i>=0){
		bound[i]=bound[i+1]+y_points[i+1]*(x_points[i+1]-x_points[i]);
		i--;
	}
	/*
	for(i=0; i<n; i++){
		printf("%.12f, ",bound[i]);
	}
	printf("\n");
	for(i=0; i<n; i++){
		printf("(%.12f, %.12f) ",x_points[i],y_points[i]);
	}
	fflush(stdout); */
	
	maximizer(0.000000000000,0,k,130);
	printf("%.12f\n",best);
	
	return 0;
}

/*funcao recursiva*/
void maximizer(double area_now, int offset, int k_left, int last_point_picked){
	
	if (area_now > best - 0.00000000000001){
		best=area_now;
	}
	
	if (k_left==0 || offset>n){
		return;
	}
	
	if (area_now + y_points[offset]*(x_points[offset] - x_points[last_point_picked]) + bound[offset] < best - 0.000000000001){
		/*printf("bounded\n");*/
		return;
	}
	
	maximizer(area_now + y_points[offset]*(x_points[offset] - x_points[last_point_picked]), offset+1, k_left-1, offset);
	maximizer(area_now, offset+1, k_left, last_point_picked);
	
	return;
}

/*source: https://www.cprogramming.com/tutorial/computersciencetheory/merge.html
	adapted to deal with doubles */
	
/* Helper function for finding the max of two numbers */
double max(double x, double y)
{
    if(x > y- 0.00000000001)
    {
        return x;
    }
    else
    {
        return y;
    }
}

/* left is the index of the leftmost element of the subarray; right is one
 * past the index of the rightmost element */
void merge_helper(double *input, int left, int right, double *scratch)
{
    /* base case: one element */
    if(right == left + 1)
    {
        return;
    }
    else
    {
        int i = 0;
        int length = right - left;
        int midpoint_distance = length/2;
        /* l and r are to the positions in the left and right subarrays */
        int l = left, r = left + midpoint_distance;

        /* sort each subarray */
        merge_helper(input, left, left + midpoint_distance, scratch);
        merge_helper(input, left + midpoint_distance, right, scratch);

        /* merge the arrays together using scratch for temporary storage */ 
        for(i = 0; i < length; i++)
        {
            /* Check to see if any elements remain in the left array; if so,
             * we check if there are any elements left in the right array; if
             * so, we compare them.  Otherwise, we know that the merge must
             * use take the element from the left array */
            if(l < left + midpoint_distance && 
                    (r == right || fabs(max(input[l], input[r])-input[l])<0.00000000001) )
            {
                scratch[i] = input[l];
                extra[i]=x_points[l];
				l++;
            }
            else
            {
                scratch[i] = input[r];
                extra[i]=x_points[r];
                r++;
            }
        }
        /* Copy the sorted subarray back to the input */
        for(i = left; i < right; i++)
        {
            input[i] = scratch[i - left];
            x_points[i]=extra[i-left];
        }
    }
}

/* mergesort returns true on success.  Note that in C++, you could also
 * replace malloc with new and if memory allocation fails, an exception will
 * be thrown.  If we don't allocate a scratch array here, what happens? 
 *
 * Elements are sorted in reverse order -- greatest to least */

int mergesort(double *input, int size)
{
    double *scratch = (double *)malloc(size * sizeof(double));
    if(scratch != NULL)
    {
        merge_helper(input, 0, size, scratch);
        free(scratch);
        return 1;
    }
    else
    {
        return 0;
    }
}
