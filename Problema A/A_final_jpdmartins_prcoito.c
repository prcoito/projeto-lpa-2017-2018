#include <stdio.h>
#include <string.h>

#define DEBUG 0
#define ACEITEI 0
#define CONTADOR 0
#define REM 0

typedef struct rectangulo
{
	int largura;
	int altura;
	int quadrado;
	int numsemelhantes;

} Rectangulo;

int num_rectangles;
int num_rectangles_dif=0;
Rectangulo rectangles[8];
int area_maxima_autorizada=0;

int solucoes[10000][16][2];
int num_solucoes=0;

#if DEBUG || CONTADOR
int avanco=0;
#endif

typedef struct construcao{
	
	int vertices_para_colocar[9][2];
	int num_vertices_para_colocar;
	
	int vertices_colocados[16][2];		/* por cada rectangulo guardam-se 2 pontos */
	int num_rectangulos_adicionados;
	
	int x_max;
	int y_max;

} Construcao;


void printRectangles_array(){
	int i=0;
	while (i<num_rectangles_dif){
		printf("%d por %d e area %d e existem %d iguais\n",rectangles[i].altura,rectangles[i].largura,rectangles[i].altura*rectangles[i].largura, rectangles[i].numsemelhantes);
		i++;
	}
}

void print_array(int array[],int size){
	int i=0;
	printf("[ ");
	while (i<size){
		printf("%d ", array[i]);
		i++;
	}
	printf("]\n");
}


int constroiRetangulo(Construcao c_atual){
	int ii, jj, kk, stop;
	int numVerticesComuns = 0;
			   /* c_atual.num_rectangulos_adicionados <=> numVertices <=> 2 * num_rectangulos */
	if (c_atual.num_rectangulos_adicionados==num_rectangles*2){	/* Guardamos 2 vertices por rect */
		
		if( c_atual.num_vertices_para_colocar==2){
			ii = 0;
			while ( ii < num_solucoes && numVerticesComuns != num_rectangles )	/* numVericesComuns <=> numFigComuns */
			{
				numVerticesComuns=0;
				jj = 0;
				while ( jj < num_rectangles*2 && numVerticesComuns *2 == jj){
					kk = 0;
					stop = 0;
					while (kk < num_rectangles *2 && !stop) {
						#if DEBUG | REM
						printf("Iteracao %d/%d/%d PontoAVerificar (%d, %d) =?= PontoValido (%d, %d) ", ii, jj, kk, c_atual.vertices_colocados[kk][0],  c_atual.vertices_colocados[kk][1], solucoes[ii][jj][0], solucoes[ii][jj][1]);
						printf("Iteracao %d/%d/%d PontoAVerificar (%d, %d) =?= PontoValido (%d, %d) ", ii, jj, kk, c_atual.vertices_colocados[kk+1][0],  c_atual.vertices_colocados[kk+1][1], solucoes[ii][jj+1][0], solucoes[ii][jj+1][1]);
						#endif

						if ( solucoes[ii][jj][0] == c_atual.vertices_colocados[kk][0] && 		/* Ponto Inferior Esquerdo x */
							 solucoes[ii][jj][1] == c_atual.vertices_colocados[kk][1] &&		/* Ponto Inferior Esquerdo y */
							 solucoes[ii][jj+1][0] == c_atual.vertices_colocados[kk+1][0] && 	/* Ponto Superior Direito x */
							 solucoes[ii][jj+1][1] == c_atual.vertices_colocados[kk+1][1] ){	/* Ponto Superior Direito x */
							numVerticesComuns++;
							stop = 1;
							#if DEBUG | REM
							printf("sim\n");
							#endif
						}
						else{
							#if DEBUG | REM
							printf("nao\n");
							#endif
						}
						kk+=2;
					}
					jj+=2;
					#if DEBUG | REM
					printf("\n========================================\n\n");
					#endif
				}
				ii++;
				#if DEBUG | REM
				printf("\n############################################\n\n");
				#endif
			}
			if ( numVerticesComuns != num_rectangles)
			{
				#if DEBUG | REM
				printf("Nova solucao encontrada:\n\t[ (%d, %d)", c_atual.vertices_colocados[0][0], c_atual.vertices_colocados[0][1]);
				jj = 1;
				while ( jj < num_rectangles ){
					printf(", (%d, %d)", c_atual.vertices_colocados[jj][0], c_atual.vertices_colocados[jj][1]);
					jj++;
				}
				printf("]\n\n\n");
				#endif
				memcpy(solucoes[num_solucoes++], c_atual.vertices_colocados, sizeof(c_atual.vertices_colocados));
				#if ACEITEI | REM
				printf("Area aceite = %d\n", c_atual.x_max * c_atual.y_max );
				#endif
				#if DEBUG | REM
				printf("\t\t\tACEITEI ESTA COMBINATION DA TRETAS\n");
				#endif
				return 1;
			}
			else{
				#if DEBUG | REM
				printf("Solucao duplicada\n");
				#endif
				return 0;
			}
		}
		else{
			return 0;
		}
	
		
	}

	int x_primeiro_ponto,y_primeiro_ponto,x_segundo_ponto,y_segundo_ponto;
	
	
	
	int rectangulo,vertice=0,contador=0,skip;
	#if DEBUG
	int i;
	printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
	printf("=========================================\n");
	#endif
	while(vertice<c_atual.num_vertices_para_colocar){
		
		skip=0;
		/*if(vertice==1 && c_atual.num_vertices_para_colocar==2 && c_atual.vertices_para_colocar[0][1]==c_atual.vertices_para_colocar[1][0] && c_atual.vertices_para_colocar[0][0]==c_atual.vertices_para_colocar[1][1]){
		#if SKIP	
			printf("DEI SKIP\n");
		#endif
			skip=1;
		}
		*/
		if(!skip){
			#if DEBUG
			printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
			printf("\tVertice: %d %d\n", c_atual.vertices_para_colocar[vertice][0],c_atual.vertices_para_colocar[vertice][1]);
			#endif
			rectangulo=0;
			while(rectangulo < num_rectangles_dif){
				#if DEBUG
				printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
				printf("\t\tRetangulo <%d> com <%d> ainda semelhantes [%d %d]\n", rectangulo, rectangles[rectangulo].numsemelhantes, rectangles[rectangulo].largura,rectangles[rectangulo].altura);
				#endif
				int x_max_temp=c_atual.x_max;
				int y_max_temp=c_atual.y_max;
				#if DEBUG
				printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
				printf("\t\tx_max_temp %d, y_max_temp %d\n", x_max_temp, y_max_temp);
				#endif
				
				if(rectangles[rectangulo].numsemelhantes!=0){
					
					skip=0;
					
					rectangles[rectangulo].numsemelhantes--;		
					
					/*tou no vertice e meto o retangulo... altura para cima e largura para o lado
						
					calculo os dois novos pontos
					(primeiro ponto para cima e segundo para o lado)*/
					x_primeiro_ponto= c_atual.vertices_para_colocar[vertice][0];
					y_primeiro_ponto= c_atual.vertices_para_colocar[vertice][1]+ rectangles[rectangulo].altura;
					#if DEBUG
					printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
					printf("\t\t\t1o ponto calculado = (%d, %d)\n", x_primeiro_ponto, y_primeiro_ponto);
					#endif
					x_segundo_ponto= c_atual.vertices_para_colocar[vertice][0]+ rectangles[rectangulo].largura;
					y_segundo_ponto= c_atual.vertices_para_colocar[vertice][1];
					#if DEBUG
					printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
					printf("\t\t\t2o ponto calculado = (%d, %d)\n", x_segundo_ponto, y_segundo_ponto);
					#endif
					/*
					se vertice atual x=0 e y=0:
						tudo bem ?normal tou no inicio;*/
					if (c_atual.vertices_para_colocar[vertice][0]==0 && c_atual.vertices_para_colocar[vertice][1]==0 ){
						x_max_temp=x_segundo_ponto;
						y_max_temp=y_primeiro_ponto;
					}	
					/*	
					se vertice atual tiver y a 0:
						x_max_temp
						testo se y do  primeiro ponto q calculei ?maior q ymax || x_max_temp * y_max da construcao > area_autorizada
							se sim:
								passo ?orientacao seguinte
							else:
								atualizar X max*/
					else if(c_atual.vertices_para_colocar[vertice][1]==0){ 
						if(y_primeiro_ponto > c_atual.vertices_para_colocar[vertice-1][1] || x_segundo_ponto * y_max_temp > area_maxima_autorizada){
							
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\tExclui por vertice atual estar com y a 0\n");
							#endif
							skip=1;	
						}
					}
					/*			
					se vertice atual tiver x a 0:
						y_max_temp
						testo se x do segundo ponto q calculei ?maior q xmax || y_max_temp * x_max da construcao > area_autorizada
							se sim:
								passo ?orientacao seguinte
							else:
								atualizar y max*/
					else if(c_atual.vertices_para_colocar[vertice][0]==0){
						if((x_segundo_ponto > c_atual.vertices_para_colocar[vertice+1][0]) || (y_primeiro_ponto * x_max_temp > area_maxima_autorizada)){
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\tExclui por vertice atual estar com x a 0\n");
							#endif
							skip=1;
						}				
					}
					/*
					se vertice atual x!= 0 e y!=0:
						testo se y do primeiro ponto q calculei ?maior q ymax e
						testo se x do segundo ponto q calculei ?maior q x_max
							se sim:
								passo ?orientacao seguinte*/
					else if(c_atual.vertices_para_colocar[vertice][0]!=0 && c_atual.vertices_para_colocar[vertice][1]!=0){
						if(y_primeiro_ponto > c_atual.vertices_para_colocar[vertice-1][1] || x_segundo_ponto > c_atual.vertices_para_colocar[vertice+1][0] ){/*estava com && aqui e nao pode ser*/
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\tExclui por vertice atual estar com x e y != 0\n");
							#endif
							skip=1;
						}							
					}
					
					/*caso nao seja de ignorar, ha que chamar a funcao (finalmente)*/
					if(!skip){
						Construcao c_temp;
						memcpy(&c_temp,&c_atual,sizeof(c_atual));
						
						
						c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados][0]=c_temp.vertices_para_colocar[vertice][0]; /* Ponto Inferior Esquerdo x */
						c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados][1]=c_temp.vertices_para_colocar[vertice][1]; /* Ponto Inferior Esquerdo y */
						c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados+1][0]=x_segundo_ponto; /* Ponto Superior Direito x */
						c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados+1][1]=y_primeiro_ponto; /* Ponto Superior Direito y */
						
						c_temp.num_rectangulos_adicionados+=2;
						
						
						/*testar se:
							-> se o y_primeiro ponto = y_max ->> nao adiciono o primeiro ponto
							
							-> se o x_segundo_potno = x_max ->> nao adiciono o segundo ponto*/
						int posicoes_a_mover=-1, copiar_primeiro_vertice=0,copiar_segundo_vertice=0;
						/*if (y_primeiro_ponto != c_atual.y_max){*/
						if (c_atual.vertices_para_colocar[vertice][0]==0 || y_primeiro_ponto != c_atual.vertices_para_colocar[vertice-1][1]){
							copiar_primeiro_vertice=1;	
						}
						/*if (x_segundo_ponto != c_atual.x_max){*/
						if (c_atual.vertices_para_colocar[vertice][1]==0 || x_segundo_ponto != c_atual.vertices_para_colocar[vertice+1][0]){	
							copiar_segundo_vertice=1;
						}
						posicoes_a_mover=posicoes_a_mover+copiar_primeiro_vertice+copiar_segundo_vertice;
						
						/*retirar o vertice onde calculamos*/
						#if DEBUG
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\tA retirar o vertice (%d, %d)\n", c_temp.vertices_para_colocar[vertice][0], c_temp.vertices_para_colocar[vertice][1]);
						#endif
						
						if(posicoes_a_mover==-1){
							int contador_temporario=vertice;
							while(contador_temporario<c_temp.num_vertices_para_colocar-1){
								c_temp.vertices_para_colocar[contador_temporario][0]=c_temp.vertices_para_colocar[contador_temporario+1][0];
								c_temp.vertices_para_colocar[contador_temporario][1]=c_temp.vertices_para_colocar[contador_temporario+1][1];
								contador_temporario++;
							}
						}
						else if (posicoes_a_mover==0){
							if(copiar_primeiro_vertice){
								c_temp.vertices_para_colocar[vertice][0]=x_primeiro_ponto;
								c_temp.vertices_para_colocar[vertice][1]=y_primeiro_ponto;
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_primeiro_ponto, y_primeiro_ponto);
								#endif	
							}
							else{
								c_temp.vertices_para_colocar[vertice][0]=x_segundo_ponto;
								c_temp.vertices_para_colocar[vertice][1]=y_segundo_ponto;
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_segundo_ponto, y_segundo_ponto);
								#endif
							}
						}
						else{
							int contador_temporario=c_temp.num_vertices_para_colocar;
							while(contador_temporario>vertice){
								c_temp.vertices_para_colocar[contador_temporario][0]=c_temp.vertices_para_colocar[contador_temporario-1][0];
								c_temp.vertices_para_colocar[contador_temporario][1]=c_temp.vertices_para_colocar[contador_temporario-1][1];
								contador_temporario--;
							}
							c_temp.vertices_para_colocar[vertice][0]=x_primeiro_ponto;
							c_temp.vertices_para_colocar[vertice][1]=y_primeiro_ponto;
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_primeiro_ponto, y_primeiro_ponto);
							#endif	
							c_temp.vertices_para_colocar[vertice+1][0]=x_segundo_ponto;
							c_temp.vertices_para_colocar[vertice+1][1]=y_segundo_ponto;
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_segundo_ponto, y_segundo_ponto);
							#endif
							
						}
						
					
						c_temp.num_vertices_para_colocar+=posicoes_a_mover;
						
						
						c_temp.x_max=c_temp.vertices_para_colocar[c_temp.num_vertices_para_colocar-1][0];
						c_temp.y_max=c_temp.vertices_para_colocar[0][1];
						
						#if DEBUG
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\tArray de vertices: [");
						for (i = 0; i<c_temp.num_vertices_para_colocar; i++)
							printf("(%d, %d), ", c_temp.vertices_para_colocar[i][0], c_temp.vertices_para_colocar[i][1]);
						printf("]\n");
						
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("O valor maximo a passar de x %d e ", c_temp.x_max);
						printf("O valor maximo a passar de y %d\n" , c_temp.y_max);
						
						#endif
						
						#if DEBUG || CONTADOR
						avanco+=3;
						#endif
						#if CONTADOR
						printf("%d\n",avanco/3);
						#endif
						contador+=constroiRetangulo(c_temp);/*//uma orientacao*/
						#if DEBUG || CONTADOR
						avanco-=3;
						#endif
					
						
					
					}		
					
					if(!rectangles[rectangulo].quadrado){
						
						#if DEBUG							
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\t+-------------------+\n");
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\t|A iniciar a rotacao do retangulo %d com [%d %d] |\n\n",rectangulo, rectangles[rectangulo].largura,rectangles[rectangulo].altura);
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\t+-------------------+\n");
						#endif
						/* 
						imaginar a rotacao
						*/
						
						skip=0;		
						
						x_max_temp=c_atual.x_max;
					 	y_max_temp=c_atual.y_max;
						/*tou no vertice e meto o retangulo... largura para cima e altura para o lado
							
						calculo os dois novos pontos
						(primeiro ponto para cima e segundo para o lado)*/
						x_primeiro_ponto= c_atual.vertices_para_colocar[vertice][0];
						y_primeiro_ponto= c_atual.vertices_para_colocar[vertice][1]+ rectangles[rectangulo].largura;
						#if DEBUG
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\t1o ponto calculado = (%d, %d)\n", x_primeiro_ponto, y_primeiro_ponto);
						#endif
						x_segundo_ponto= c_atual.vertices_para_colocar[vertice][0]+ rectangles[rectangulo].altura;
						y_segundo_ponto= c_atual.vertices_para_colocar[vertice][1];
						#if DEBUG
						printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
						printf("\t\t\t2o ponto calculado = (%d, %d)\n", x_segundo_ponto, y_segundo_ponto);
						#endif
						/*
						se vertice atual x=0 e y=0:
							tudo bem ?normal tou no inicio;*/
						if (c_atual.vertices_para_colocar[vertice][0]==0 && c_atual.vertices_para_colocar[vertice][1]==0 ){
							x_max_temp=x_segundo_ponto;
							y_max_temp=y_primeiro_ponto;
						}	
						/*	
						se vertice atual tiver y a 0:
							x_max_temp
							testo se y do  primeiro ponto q calculei ?maior q ymax || x_max_temp * y_max da construcao > area_autorizada
								se sim:
									passo ?orientacao seguinte
								else:
									atualizar X max*/
						else if(c_atual.vertices_para_colocar[vertice][1]==0){ 
							if(y_primeiro_ponto > c_atual.vertices_para_colocar[vertice-1][1] || x_segundo_ponto * y_max_temp > area_maxima_autorizada){
								
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\tExclui por vertice atual estar com y a 0\n");
								#endif
								skip=1;	
							}						
						}
						/*			
						se vertice atual tiver x a 0:
							y_max_temp
							testo se x do segundo ponto q calculei ?maior q xmax || y_max_temp * x_max da construcao > area_autorizada
								se sim:
									passo ?orientacao seguinte
								else:
									atualizar y max*/
						else if(c_atual.vertices_para_colocar[vertice][0]==0){
							if(x_segundo_ponto > c_atual.vertices_para_colocar[vertice+1][0] || y_primeiro_ponto * x_max_temp > area_maxima_autorizada){
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\tExclui por vertice atual estar com x a 0\n");
								#endif
								skip=1;
							}						
						}
						/*
						se vertice atual x!= 0 e y!=0:
							testo se y do primeiro ponto q calculei ?maior q ymax e
							testo se x do segundo ponto q calculei ?maior q x_max
								se sim:
									passo ?orientacao seguinte*/
						else if(c_atual.vertices_para_colocar[vertice][0]!=0 && c_atual.vertices_para_colocar[vertice][1]!=0){
							if(y_primeiro_ponto > c_atual.vertices_para_colocar[vertice-1][1] || x_segundo_ponto > c_atual.vertices_para_colocar[vertice+1][0] ){
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\tExclui por vertice atual estar com x e y != 0\n");
								#endif
								skip=1;
							}							
						}
						
						/*caso nao seja de ignorar, ha que chamar a funcao (finalmente)*/
						if(!skip){
							Construcao c_temp;
							memcpy(&c_temp,&c_atual,sizeof(c_atual));
							
							
						
							c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados][0]=c_temp.vertices_para_colocar[vertice][0]; /* Ponto Inferior Esquerdo x */
							c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados][1]=c_temp.vertices_para_colocar[vertice][1]; /* Ponto Inferior Esquerdo y */
							c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados+1][0]=x_segundo_ponto; /* Ponto Superior Direito x */
							c_temp.vertices_colocados[c_temp.num_rectangulos_adicionados+1][1]=y_primeiro_ponto; /* Ponto Superior Direito y */
							
							c_temp.num_rectangulos_adicionados+=2;
							
							/*testar se:
								-> se o y_primeiro ponto = y_max ->> nao adiciono o primeiro ponto
								
								-> se o x_segundo_potno = x_max ->> nao adiciono o segundo ponto
								
								 if(w < xf and h < yf):
            						c = c[:i] + ((xi,h),(w,yi)) + c[i+1:]        
        						elif(w == xf and h != yf):
            						c = c[:i] + ((xi,h),) + c[i+1:]
        						elif(w != xf and h == yf):
          							 c = c[:i] + ((w,yi),) + c[i+1:]
        						elif(w == xf and h == yf):
            						c = c[:i] + c[i+1:]
								
								
								*/
							int posicoes_a_mover=-1, copiar_primeiro_vertice=0,copiar_segundo_vertice=0;
							/*if (y_primeiro_ponto != c_atual.y_max){*/
							if (c_atual.vertices_para_colocar[vertice][0]==0 || y_primeiro_ponto != c_atual.vertices_para_colocar[vertice-1][1]){
								copiar_primeiro_vertice=1;	
							}
							/*if (x_segundo_ponto != c_atual.x_max){*/
							if (c_atual.vertices_para_colocar[vertice][1]==0 || x_segundo_ponto != c_atual.vertices_para_colocar[vertice+1][0]){	
								copiar_segundo_vertice=1;
							}
							posicoes_a_mover=posicoes_a_mover+copiar_primeiro_vertice+copiar_segundo_vertice;
							
							/*retirar o vertice onde calculamos*/
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\t\tA retirar o vertice (%d, %d)\n", c_temp.vertices_para_colocar[vertice][0], c_temp.vertices_para_colocar[vertice][1]);
							#endif
							
							
							if(posicoes_a_mover==-1){
								int contador_temporario=vertice;
								while(contador_temporario<c_temp.num_vertices_para_colocar-1){
									c_temp.vertices_para_colocar[contador_temporario][0]=c_temp.vertices_para_colocar[contador_temporario+1][0];
									c_temp.vertices_para_colocar[contador_temporario][1]=c_temp.vertices_para_colocar[contador_temporario+1][1];
									contador_temporario++;
								}
							}
							else if (posicoes_a_mover==0){
								if(copiar_primeiro_vertice){
									c_temp.vertices_para_colocar[vertice][0]=x_primeiro_ponto;
									c_temp.vertices_para_colocar[vertice][1]=y_primeiro_ponto;
									#if DEBUG
									printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
									printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_primeiro_ponto, y_primeiro_ponto);
									#endif	
								}
								else{
									c_temp.vertices_para_colocar[vertice][0]=x_segundo_ponto;
									c_temp.vertices_para_colocar[vertice][1]=y_segundo_ponto;
									#if DEBUG
									printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
									printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_segundo_ponto, y_segundo_ponto);
									#endif
								}
							}
							else{
								int contador_temporario=c_temp.num_vertices_para_colocar;
								while(contador_temporario>vertice){
									c_temp.vertices_para_colocar[contador_temporario][0]=c_temp.vertices_para_colocar[contador_temporario-1][0];
									c_temp.vertices_para_colocar[contador_temporario][1]=c_temp.vertices_para_colocar[contador_temporario-1][1];
									contador_temporario--;
								}
								c_temp.vertices_para_colocar[vertice][0]=x_primeiro_ponto;
								c_temp.vertices_para_colocar[vertice][1]=y_primeiro_ponto;
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_primeiro_ponto, y_primeiro_ponto);
								#endif	
								c_temp.vertices_para_colocar[vertice+1][0]=x_segundo_ponto;
								c_temp.vertices_para_colocar[vertice+1][1]=y_segundo_ponto;
								#if DEBUG
								printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
								printf("\t\t\tAdicionou vertice em (%d, %d)\n", x_segundo_ponto, y_segundo_ponto);
								#endif
								
							}
						
							c_temp.num_vertices_para_colocar+=posicoes_a_mover;
							
							c_temp.x_max=c_temp.vertices_para_colocar[c_temp.num_vertices_para_colocar-1][0];
							c_temp.y_max=c_temp.vertices_para_colocar[0][1];
							
							#if DEBUG
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("\t\t\tArray de vertices: [");
							for (i = 0; i<c_temp.num_vertices_para_colocar; i++)
								printf("(%d, %d), ", c_temp.vertices_para_colocar[i][0], c_temp.vertices_para_colocar[i][1]);
							printf("]\n");
							
							printf("%.*s",avanco,"\t\t\t\t\t\t\t\t\t\t\t\t\t");
							printf("O valor maximo a passar de x %d e ", c_temp.x_max);
							printf("O valor maximo a passar de y %d\n" , c_temp.y_max);
						
							#endif
						
							#if DEBUG || CONTADOR
							avanco+=3;
							#endif
							#if CONTADOR
							printf("%d\n",avanco/3);
							#endif
							contador+=constroiRetangulo(c_temp);/*//uma orientacao*/
							#if DEBUG || CONTADOR
							avanco-=3;
							#endif
						
							
						}
					}
					rectangles[rectangulo].numsemelhantes++;
				}
				rectangulo++;
			}
		}	
		vertice++;
	}
	
	return contador;
}

typedef struct vertice
{
	int x;
	int y;
	int matrix[8][2];
	/*
	posicao atual no quadrante (será que vale a pena?)
	*/
} Vertice;

int main(){
	
	
	scanf("%d", &num_rectangles);
	/*printf("%d\n",num_rectangles);*/
	int i=0,j=0, not_found;
	
	/*num_rectangles=0;*/
	
	while(i<num_rectangles){
		not_found=1;
		int altura,largura;
		scanf("%d %d",&altura,&largura);
		area_maxima_autorizada += largura*altura;

		j=0;
		while(j<num_rectangles_dif && not_found){
			if((largura==rectangles[j].largura && altura==rectangles[j].altura)|| 
			   (altura==rectangles[j].largura && largura==rectangles[j].altura)   ){
				
				rectangles[j].numsemelhantes++;
				not_found=0;
			}
			j++;
		}
		if (not_found){
			rectangles[num_rectangles_dif].altura=altura;
			rectangles[num_rectangles_dif].largura=largura;
			rectangles[num_rectangles_dif].numsemelhantes=1;
			rectangles[i].quadrado=(altura==largura)?1:0;
			
			num_rectangles_dif++;
		}
		
		i++;
	}
	#if DEBUG || ACEITEI
	printRectangles_array();
	printf("%d\n",area_maxima_autorizada);
	#endif
	Construcao c1;
	
	c1.num_rectangulos_adicionados=0;
	
	c1.vertices_para_colocar[0][0]=0;
	c1.vertices_para_colocar[0][1]=0;
	c1.num_vertices_para_colocar=1;
	c1.x_max=0;
	c1.y_max=0;

	/*constroiRetangulo(c1);*/
	printf("%d\n", constroiRetangulo(c1));
	/*printf("%d\n", num_solucoes);*/
	return 0;
	
	/*
	Vertice v1;
	Vertice v2;
	v1.x=2;
	v2.x=2;
	v1.y=3;
	v2.y=3;
	
	v1.matrix[0][0]=1;
	v1.matrix[0][1]=3;
	int matrix1[][2]={1,2};
	int matrix2[8][2];
	
	matrix2[0][0]=1;
	matrix2[0][1]=3;
	
	Vertice array1[1];
	Vertice array2[1];
	array1[0]=v1;
	array2[0]=v2;
	printf("teste de comparar: %d\n", memcmp(&matrix2, &v1.matrix, sizeof(matrix2[0])));*/
	
}
