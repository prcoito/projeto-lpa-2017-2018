#include <stdio.h>
#include <math.h>



/* ax + by + c = 0 */
typedef struct {
    double a; /* x-coefficient */
    double b; /* y-coefficient */
    double c; /* constant term */
} line;

typedef struct{
    double x;
    double y;
} point ;

void points_to_line(point *p1, point *p2, line *l)
{
    if (p1->x == p2->x) {
        l->a = 1;
        l->b = 0;
        l->c = -p1->x;
    } else {
        l->b = 1;
        l->a = -(p1->y-p2->y)/(p1->x-p2->x);
        l->c = -(l->a * p1->x) - (l->b * p1->y);
    }
}

int parallelQ(line *l1, line *l2){
	return ( (fabs(l1->a-l2->a) <= 0.00001) && (fabs(l1->b-l2->b) <= 0.00001) );
}


void intersection_point(line *l1, line *l2, point *p)
{
    /*
    if (same_lineQ(l1,l2)) {
    printf("Warning: Identical lines, all points intersect.\n");
    p->x = p->y = 0.0;
    return;
    }
    */
    if ( parallelQ(l1,l2) ) {
        printf("Error: Distinct parallel lines do not intersect.\n");
        return;
    }
    
    p->x = (l2->b*l1->c - l1->b*l2->c) / (l2->a*l1->b - l1->a*l2->b);
    if (fabs(l1->b) > 0.0000001) /* test for vertical line */
        p->y = - (l1->a * (p->x) + l1->c) / l1->b;
    else
        p->y = - (l2->a * (p->x) + l2->c) / l2->b;
}

int main()
{
    point *p1 = (point *)malloc(sizeof(point));
    point *p2 = (point *)malloc(sizeof(point));
    point *p3 = (point *)malloc(sizeof(point));
    point *p4 = (point *)malloc(sizeof(point));
    point *pi = (point *)malloc(sizeof(point));
    
    line *l1 = (line *)malloc(sizeof(line));
    line *l2 = (line *)malloc(sizeof(line));
    
    p1->x = 1.0;
    p1->y = 0.0;
    p2->x = 2.5;
    p2->y = 2.0;
    
    p3->x = 1.0;
    p3->y = 1.0;
    p4->x = 2.5;
    p4->y = 3.0;
    
    points_to_line(p1, p2, l1);
    points_to_line(p3, p4, l2);
    
    intersection_point(l1, l2, pi);
    
    printf("I<%lf, %lf>", pi->x, pi->y);

    return 0;
}
