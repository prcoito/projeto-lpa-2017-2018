#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <float.h>
#include <math.h>

#define DEBUG 1


/*//struct point*/
typedef struct point{
	double x;
	double y;
	int type; /*//-1 -> start     0 -> intersesao       1-> finish*/
	int line1;
	int line2;

}Point;

typedef struct _node* Node;

struct _node{
	int value;
	double value1; /*m*/
	double value2; /*b*/

	int lower_value;

	Node next;
	Node previous;
}node;

Node info_lines;
Node info_lines_back;

Node info_segment; /* aqui os pontos são os iniciais e representam uma linha*/

int n_casos;
int n_lines;

double x_in1,y_in1,x_in2,y_in2;

int n_points=0;
Point points[1002001];

Point line_points[2002];


int compare (const void * a, const void * b){

	Point* _a = (Point*)a;
	Point* _b = (Point*)b;
	if ( _a->x <  _b->x ) return -1;
  	if ( fabs(_a->x - _b->x) < 0.00000001 ){/*0.00000000000000000000001 ou DBL_EPSILON*/
  		if ( _a->y <  _b->y ) return -1;
  		if ( _a->y == _b->y ) return 0;
  		if ( _a->y >  _b->y ) return 1;
  	}
  	if ( _a->x >  _b->x ) return 1;

  	return 69;
}
int doIntersect(Point p1, Point q1, Point p2, Point q2);




int main(){
	
	/*vars*/
	int line;
	int compare_line;
	double denominator;
	double intersect_x,intersect_y;
	double x1,x2,x3,x4,y1,y2,y3,y4;
	double _x1,_y1,_x2,_y2;
	int current_point;	
	int contador_posicao;
	Node temporary;
	double y_current_pos;
	int curr_pos;
	int to_store_value;

	/*scanf casos de teste*/
	scanf("%d",&n_casos);

	/*while casos de testes*/
	int caso=0;
	while(caso<n_casos){
		
		/*//printf("\nCaso n: %d\n", caso);*/

		/*scanf x1 y1 x2 y2*/
		scanf("%lf %lf %lf %lf",&x_in1,&y_in1,&x_in2,&y_in2);

		/*scanf n_lines*/
		scanf("%d",&n_lines);

		/*while n_lines*/
		n_points=0;
		while(n_points/2< n_lines){

			
			/*scanf x_start y_start x_final y_final*/
			scanf("%lf %lf %lf %lf",&_x1,&_y1,&_x2,&_y2);

			
			if(_x1<_x2 || ( _x1==_x2 && _y1<_y2)){
				/*scanf("%lf %lf", &points[n_points].x, &points[n_points].y);*/
				points[n_points].x=_x1;
				points[n_points].y=_y1;
				points[n_points].type = -1;
				points[n_points].line1 = n_points/2;

				n_points++;
		
				/*scanf("%lf %lf", &points[n_points].x, &points[n_points].y);*/
				points[n_points].x=_x2;
				points[n_points].y=_y2;
				points[n_points].type = 1;
				points[n_points].line1 = n_points/2;

				n_points++;
			
			}
			else{
				/*scanf("%lf %lf", &points[n_points].x, &points[n_points].y);*/
				points[n_points].x=_x2;
				points[n_points].y=_y2;
				points[n_points].type = -1;
				points[n_points].line1 = n_points/2;

				n_points++;
		
				/*scanf("%lf %lf", &points[n_points].x, &points[n_points].y);*/
				points[n_points].x=_x1;
				points[n_points].y=_y1;
				points[n_points].type = 1;
				points[n_points].line1 = n_points/2;

				n_points++;
			
			}

		}
		/*
		for (int q=0;q<n_points-1; q+=2){
			printf("Line %d Starts <%lf %lf> and Finishs <%lf %lf>\n", q/2, points[q].x, points[q].y, points[q+1].x, points[q+1].y);
		}
		*/
		memcpy(line_points, points,n_lines*2*sizeof(Point));

		/*
		printf("\n");		
		for (int q=0;q<n_points-1; q+=2){
			printf("Line %d Starts <%lf %lf> and Finishs <%lf %lf>\n", q/2, line_points[q].x, line_points[q].y, line_points[q+1].x, line_points[q+1].y);
		}
		*/



		line=0;
		while(line< n_lines){
			compare_line=line+1;
			while(compare_line< n_lines){
				x1=points[line*2].x;
				y1=points[line*2].y;
				
				x2=points[line*2+1].x;
				y2=points[line*2+1].y;

				x3=points[compare_line*2].x;
				y3=points[compare_line*2].y;
				
				x4=points[compare_line*2+1].x;
				y4=points[compare_line*2+1].y;


				 /*x1 <= x4  && x2 >= x3  && y1 <= y4  && y2 >= y3 &&*/
				if(	doIntersect(points[line*2], points[line*2+1], points[compare_line*2], points[compare_line*2+1])){
					/*//add point*/
					denominator= ((y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1));
					if(denominator==0){
						/*printf("should never happen\n");*/
					}
					/*else{*/
					double ua= (( x4 - x3 )*( y1 - y3 ) - ( y4 - y3 )*( x1 - x3 ))/ denominator;
					intersect_x= x1 + ua*( x2 - x1 );
					intersect_y= y1 + ua*( y2 - y1 );
					#if DEBUG
					printf("I<%lf %lf>\n",intersect_x,intersect_y );
					#endif
					/*printf("Yeah it works\n");*/
					points[n_points].x=intersect_x;
					points[n_points].y=intersect_y;
					points[n_points].type=0;
					points[n_points].line1=line;
					points[n_points].line2=compare_line;
					n_points++;
				}

				compare_line++;
			}
			line++;
		}

		qsort (points, n_points, sizeof(Point), compare);
		
		/*
		for (int q=0;q<n_points; q++){
			printf("<%lf %lf> of type <%d>\n", points[q].x, points[q].y,points[q].type );
		}

		printf("\n");
		*/
		
		info_segment= (Node) malloc(sizeof(node));
		info_segment->value=0;
		info_segment->next=NULL;

		info_lines=NULL;
		info_lines_back=NULL;

		current_point=0;
		while(current_point<n_points){
			#if DEBUG
			printf("$$ current_point <%lf %lf> do tipo <%d> para a linha <%d >\n",points[current_point].x,points[current_point].y, points[current_point].type, points[current_point].line1 );
			#endif

			if(points[current_point].type== -1){/* start*/

				contador_posicao=0;
				temporary=info_lines;

				while(temporary){

					/*calcular o y*/

					/*printf("\tLine being compared: <%d> starting <%lf %lf> and finishing <%lf %lf>\n", compare,x1,y1,x2,y2);*/
					y_current_pos= points[current_point].x * temporary->value1 + temporary->value2;

					/*printf("\ty_current_pos: %lf\n",y_current_pos);*/

					if(y_current_pos> points[current_point].y ){ /*inserir com linhas já existentes*/

						/*printf("\tinseri com linhas ja existentes para a frente\n");*/
						x1=line_points[points[current_point].line1*2].x;
						y1=line_points[points[current_point].line1*2].y;
					
						x2=line_points[points[current_point].line1*2+1].x;
						y2=line_points[points[current_point].line1*2+1].y;
						Node new_line= (Node) malloc(sizeof(node));
						new_line->value= points[current_point].line1;
						new_line->value1= (y2-y1)/(x2-x1);
						new_line->value2= y1 - new_line->value1 * x1;
						new_line->next=temporary;
						new_line->previous=temporary->previous;

						if(contador_posicao==0){
							info_lines=new_line;
						}
						else{
							/*printf("\tentrei no else que define o anterior\n");*/
							temporary->previous->next=new_line;
						}
						temporary->previous=new_line;
						
						break;
					}
					contador_posicao++;
					temporary=temporary->next;
				}

				if(!temporary){/*inserir no fim (mesmo q nao esteja la nada)*/
					x1=line_points[points[current_point].line1*2].x;
					y1=line_points[points[current_point].line1*2].y;
				
					x2=line_points[points[current_point].line1*2+1].x;
					y2=line_points[points[current_point].line1*2+1].y;
					Node new_line= (Node) malloc(sizeof(node));
					new_line->value= points[current_point].line1;
					new_line->value1= (y2-y1)/(x2-x1);
					new_line->value2= y1 - new_line->value1*x1;
					new_line->next=NULL;
					new_line->previous=info_lines_back;

					if(info_lines_back){
						info_lines_back->next=new_line;
					}
					if(!info_lines){
						info_lines=new_line;
						/*contador_posicao++;*/
					}

					info_lines_back=new_line;
					/*contador_posicao--;*/
				}
				#if DEBUG
				printf("\tcontador_posicao: <%d>\n",contador_posicao);
				#endif
				curr_pos=0;
				temporary=info_segment;
				while(curr_pos< contador_posicao){
					temporary=temporary->next;
					curr_pos++;
				}
				
				Node new_segment= (Node) malloc(sizeof(node));
				new_segment->next=temporary->next;
				temporary->next=new_segment;

				if(points[current_point].y == y_in1){ /*lower*/
					#if DEBUG
					printf("\tlower case insert\n");
					#endif
					new_segment->value= temporary->value;
					temporary->value=(temporary->value)+1;
				}
				else if(points[current_point].y == y_in2){ /*higher*/
					#if DEBUG
					printf("\thigher case insert\n");
					#endif
					new_segment->value= temporary->value+1;
				}
				else{ /*middle*/
					#if DEBUG
					printf("\tmiddle case insert\n");
					#endif
					new_segment->value= temporary->value;
				}

			}
			else if(points[current_point].type== 0){/* intersect*/

				contador_posicao=0;
				temporary=info_lines;

				while(temporary->next){
 
					if(temporary->value == points[current_point].line1 || temporary->value == points[current_point].line2){
						break;
					}


					contador_posicao++;
					temporary=temporary->next;
				}

				/*trocar*/										/*	A ------ B -------- C -------  D			*/
				Node to_swap=temporary->next;

				temporary->next=to_swap->next;				
				to_swap->previous=temporary->previous;
				temporary->previous=to_swap;
				to_swap->next=temporary;

				if(temporary->next){
					temporary->next->previous=temporary;
				}
				else{
					info_lines_back=temporary;
				}
				if(to_swap->previous){
					to_swap->previous->next=to_swap;
				}
				else{
					info_lines=to_swap;
				}

				/*calculos*/

				curr_pos=0;
				temporary=info_segment;
				while(curr_pos< contador_posicao){
					temporary=temporary->next;
					curr_pos++;
				}

				temporary->value= temporary->value < temporary->next->value+1 ? temporary->value : temporary->next->value+1; 
				/*por esta ordem*/
				temporary->next->next->value= temporary->next->next->value < temporary->next->value+1 ? temporary->next->next->value : temporary->next->value+1;

				temporary->next->value = temporary->value+1 < temporary->next->next->value+1 ? temporary->value+1 : temporary->next->next->value+1;
				

			}
			else if(points[current_point].type== 1){/* end*/

				contador_posicao=0;
				temporary=info_lines;

				while(temporary->next){

					if(temporary->value == points[current_point].line1){
						break;
					}

					contador_posicao++;
					temporary=temporary->next;
				}

				/*remover*/
				if(temporary->previous){
					temporary->previous->next=temporary->next;
				}
				else{
					info_lines=temporary->next;
				}
				if(temporary->next){
					temporary->next->previous=temporary->previous;
				}
				else{
					info_lines_back=temporary->previous;
				}

				free(temporary);

				curr_pos=0;
				temporary=info_segment;
				while(curr_pos< contador_posicao){
					temporary=temporary->next;
					curr_pos++;
				}
				
				if(points[current_point].y == y_in1){/*lower*/
					#if DEBUG
					printf("\tlower case remove\n");
					#endif
					to_store_value=((temporary->value+1)<temporary->next->value)?temporary->value+1:temporary->next->value;
				}
				else if(points[current_point].y == y_in2){/*higher*/
					#if DEBUG
					printf("\thigher case remove\n");
					#endif
					to_store_value=((temporary->value)<temporary->next->value+1)?temporary->value:temporary->next->value+1;
				}
				else{ /*middle*/
					#if DEBUG
					printf("\tmiddle case remove\n");
					#endif
					to_store_value=((temporary->value)<temporary->next->value)?temporary->value:temporary->next->value;
				}

				temporary->value=to_store_value;
				Node to_recicle=temporary->next;
				temporary->next=temporary->next->next;
				free(to_recicle);

				}

			}
			else{
				printf("THIS SHOULD NOT HAPPEN\n");
			}

			#if DEBUG
			printf("\tLines:( ");
			Node t=info_lines;
			while(t){
				printf("%d ", t->value);
				t=t->next;
			}
			printf(" )\n\tSegments: ( ");
			t=info_segment;
			while(t){
				printf("%d ", t->value);
				t=t->next;
			}
			printf(" )\n");
			#endif

			current_point++;
		}

		int min=info_segment->value;
		Node temp=info_segment->next;
		while(temp){
			free(temp->previous);
			
			if (temp->value < min){
				min = temp->value;
			}
			
			temp=temp->next;
		}
		printf("%d\n", min);
		
		#if DEBUG
		printf("################################\n");
		#endif

		caso++;
	}
	return 0;
}

/*
int onSegment(Point p, Point q, Point r)
{
    if (q.x <= ((p.x > r.x)?p.x:r.x) && q.x >= ((p.x < r.x)?p.x:r.x) &&
        q.y <= ((p.y > r.y)?p.y:r.y) && q.y >= ((p.y < r.y)?p.y:r.y) )
       return 1;
 
    return 0;
}*/
 /*
// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise*/
int orientation(Point p, Point q, Point r)
{
   /* // See https://www.geeksforgeeks.org/orientation-3-ordered-points/
    // for details of below formula.*/
    double val = (q.y - p.y) * (r.x - q.x) -
              (q.x - p.x) * (r.y - q.y);
 
    if (val == 0.0) return 0;  /*// colinear*/
 
    return (val > 0)? 1: 2; /*// clock or counterclock wise*/
}
 /*
// The main function that returns true if line segment 'p1q1'
// and 'p2q2' intersect.*/
int doIntersect(Point p1, Point q1, Point p2, Point q2)
{
    /*// Find the four orientations needed for general and
    // special cases*/
    int o1 = orientation(p1, q1, p2);
    int o2 = orientation(p1, q1, q2);
    int o3 = orientation(p2, q2, p1);
    int o4 = orientation(p2, q2, q1);
 
    /*// General case*/
    if (o1 != o2 && o3 != o4)
        return 1;
 	/*
    // Special Cases
    
    // p1, q1 and p2 are colinear and p2 lies on segment p1q1*/
   /* if (o1 == 0 && onSegment(p1, p2, q1)) return 1;*/
 
   /* // p1, q1 and q2 are colinear and q2 lies on segment p1q1*/
    /*if (o2 == 0 && onSegment(p1, q2, q1)) return 1;*/
 
    /*// p2, q2 and p1 are colinear and p1 lies on segment p2q2*/
    /*if (o3 == 0 && onSegment(p2, p1, q2)) return 1;*/
 
    /* // p2, q2 and q1 are colinear and q1 lies on segment p2q2*/
    /*if (o4 == 0 && onSegment(p2, q1, q2)) return 1;*/
 	
    return 0; /*// Doesn't fall in any of the above cases*/
}