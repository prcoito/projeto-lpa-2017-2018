#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define DEBUG 0

/*structs*/
typedef struct molho_de_flores
{
	int tipo;
	int numero_de_elementos;

} Molho_de_flores;


/*variáveis globais*/
int numero_de_trocas=0;
int numero_de_perdas_minimo=1500;
Molho_de_flores canteiro_em_linha[1500];
int numero_molhos_de_flores=0;

int DTABLE[300][1500];


/*declaracao de funcoes auxiliares*/
void calculador_de_perdas(int tipo_de_pesticida_atual,int offset_do_canteiro, int numero_de_trocas_restantes_atual, int numero_perdas_atual);
int calculador_de_perdas1(int tipo_de_pesticida_atual, int offset_do_canteiro, int numero_de_trocas_restantes_atual);
void print_canteiro();
void print_dtable();
void createTable(int compressed);

int main(){
	
	char flor;
	/*
	canteiro_em_linha[numero_molhos_de_flores++].numero_de_elementos=0;
	canteiro_em_linha[numero_molhos_de_flores].tipo=-1;
	*/
	while(scanf("%d ",&numero_de_trocas)==1){
		
		scanf("%c",&flor);
		canteiro_em_linha[numero_molhos_de_flores].numero_de_elementos=1;
		canteiro_em_linha[numero_molhos_de_flores].tipo=atoi(&flor);
		
		while(scanf("%c",&flor)==1){
			if(flor=='\n')
				break;
			if(canteiro_em_linha[numero_molhos_de_flores].tipo==atoi(&flor)){
				canteiro_em_linha[numero_molhos_de_flores].numero_de_elementos++;
			}
			else{
				canteiro_em_linha[++numero_molhos_de_flores].numero_de_elementos=1;
				canteiro_em_linha[numero_molhos_de_flores].tipo=atoi(&flor);
			}
			/*printf("lala\n");*/
		}
		
		/*Verificar a estrutura -> confirmed*/
		/*print_canteiro();*/
		
		/*condicoes basicas*/
		
			/*numero de trocas == ao numero de molhos*/
				/*print 0;*/
				
			/*numero de trocas == numero de molhos -1 */
				/*print minimo numero_de_elementos dum molho*/
				
			/*numero de trocas == 1*/
				/*print minimo da soma de elementos dum tipo*/
		
		#if DEBUG
		createTable(1);
		#endif

		#if DEBUG == 0
		
		/*
		calculador_de_perdas(1,0,numero_de_trocas-1,0);
		calculador_de_perdas(0,0,numero_de_trocas-1,0);
		printf("%d\n", numero_de_perdas_minimo);
		*/
		memset(DTABLE, 0, 300 * 1500 * sizeof(int));
		int a=calculador_de_perdas1(1,0,numero_de_trocas-1);
		/*print_dtable(numero_de_trocas,numero_molhos_de_flores);*/
		memset(DTABLE, 0, 300 * 1500 * sizeof(int));
		int b=calculador_de_perdas1(0,0,numero_de_trocas-1);
		/*print_dtable(numero_de_trocas,numero_molhos_de_flores);*/
		printf("%d\n",a<b?a:b); 
		#endif

		
		numero_de_trocas=0;
		numero_de_perdas_minimo=1500;
		numero_molhos_de_flores=0;
	}
	return 0;
}

/*funcoes auxiliares*/

void createTable(int compressed){
	
	Molho_de_flores canteiro_em_linha_copia[1500];
	memcpy(canteiro_em_linha_copia,canteiro_em_linha,1500*sizeof(Molho_de_flores));
	int numero_molhos_de_flores_copia=numero_molhos_de_flores;
	
	int trocas,molho,flor;
	
	for ( trocas= 1; trocas< 22; trocas++){
		int contador_flor = 0;
		for (molho = 0; molho<= numero_molhos_de_flores_copia; molho++){
			/*canteiro_em_linha[molho].tipo=canteiro_em_linha_copia[molho].tipo;*/
			numero_molhos_de_flores=molho;
			if (!compressed){
				canteiro_em_linha[molho].numero_de_elementos=0;
				for (flor = 1; flor<= canteiro_em_linha_copia[molho].numero_de_elementos; flor++)
				{
					contador_flor++;
					canteiro_em_linha[molho].numero_de_elementos++;
			
				
					if ( contador_flor <= trocas ){
						numero_de_perdas_minimo = 0;
					}
					else{
						numero_de_perdas_minimo = 1500;
						calculador_de_perdas(1,0,trocas-1,0);
						calculador_de_perdas(0,0,trocas-1,0);
					}
					printf("%02d ", numero_de_perdas_minimo);
				}
			}else{

				contador_flor+=canteiro_em_linha[molho].numero_de_elementos;
		
				if ( contador_flor < trocas ){
					numero_de_perdas_minimo = 0;
				}
				else{
					numero_de_perdas_minimo = 1500;
					calculador_de_perdas(1,0,trocas-1,0);
					calculador_de_perdas(0,0,trocas-1,0);
				}
				printf("%02d ", numero_de_perdas_minimo);
				
			}
		}
		printf("\n");
	}
}



/*funcao recursiva*/
void calculador_de_perdas(int tipo_de_pesticida_atual, int offset_do_canteiro, int numero_de_trocas_restantes_atual, int numero_perdas_atual){
	
	/*variaveis*/
	int i;
	
	/*atualizar numero de perdas*/
	if(tipo_de_pesticida_atual!=canteiro_em_linha[offset_do_canteiro].tipo){
		numero_perdas_atual+=canteiro_em_linha[offset_do_canteiro].numero_de_elementos;
	}
	/*nao vale a pena explorar o que ja é pior q o atual*/
	if(numero_perdas_atual>=numero_de_perdas_minimo){
		return;
	}
	
	/*casos base*/
	if (offset_do_canteiro==numero_molhos_de_flores || numero_de_trocas_restantes_atual >= numero_molhos_de_flores-offset_do_canteiro){
		/*pode-se retirar o if pq foi testado em cima... logo é garantido q esta solution é melhor*/
		/*if(numero_perdas_atual<numero_de_perdas_minimo){*/
			numero_de_perdas_minimo=numero_perdas_atual;
		/*}*/
		return;
	}
	else if (numero_de_trocas_restantes_atual==0){
		if(tipo_de_pesticida_atual == canteiro_em_linha[offset_do_canteiro].tipo){
			i=offset_do_canteiro+1;	
		}
		else{
			i=offset_do_canteiro+2;	
		}
		while(i<=numero_molhos_de_flores && numero_perdas_atual<numero_de_perdas_minimo){
			numero_perdas_atual+=canteiro_em_linha[i].numero_de_elementos;	
			i+=2;
		}
		if(numero_perdas_atual<numero_de_perdas_minimo){
			numero_de_perdas_minimo=numero_perdas_atual;
		}
		return;
	}
	
	/*chamada recursiva com e sem troca*/
	if (tipo_de_pesticida_atual==1){
		calculador_de_perdas(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1, numero_perdas_atual);
		calculador_de_perdas(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual, numero_perdas_atual);
	}
	else if (tipo_de_pesticida_atual==0){
		calculador_de_perdas(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual, numero_perdas_atual);
		calculador_de_perdas(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1, numero_perdas_atual);
	}
	else{
		calculador_de_perdas(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1,numero_perdas_atual);
		calculador_de_perdas(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1,numero_perdas_atual);
	}
		
	return;
}

/*funcao recursiva*/
int calculador_de_perdas1(int tipo_de_pesticida_atual, int offset_do_canteiro, int numero_de_trocas_restantes_atual){
	
	if (numero_de_trocas_restantes_atual> numero_molhos_de_flores-offset_do_canteiro){
		return 0;
	}
	
	if (DTABLE[numero_de_trocas - numero_de_trocas_restantes_atual][offset_do_canteiro]>0){
		/*printf("C ");*/
		return DTABLE[numero_de_trocas - numero_de_trocas_restantes_atual][offset_do_canteiro];
	}
	
	/*variaveis*/
	int i;
	int numero_perdas=0;
	
	/*atualizar numero de perdas*/
	if(tipo_de_pesticida_atual!=canteiro_em_linha[offset_do_canteiro].tipo){
		numero_perdas=canteiro_em_linha[offset_do_canteiro].numero_de_elementos;
	}
	
	/*casos base*/
	if (offset_do_canteiro==numero_molhos_de_flores){
		return numero_perdas;
	}
	else if (numero_de_trocas_restantes_atual==0){
		int contador=0;
		if(tipo_de_pesticida_atual == canteiro_em_linha[offset_do_canteiro].tipo){
			i=offset_do_canteiro+1;	
		}
		else{
			i=offset_do_canteiro+2;	
		}
		while(i<=numero_molhos_de_flores){ /*&& numero_perdas_atual<numero_de_perdas_minimo){*/
			contador+=canteiro_em_linha[i].numero_de_elementos;	
			i+=2;
		}
		return numero_perdas+contador;
	}
	
	int a,b;
	/*chamada recursiva com e sem troca*/
	if (tipo_de_pesticida_atual==1){
		a =	calculador_de_perdas1(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1);
		b = calculador_de_perdas1(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual);
	}
	else if (tipo_de_pesticida_atual==0){
		a = calculador_de_perdas1(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual);
		b = calculador_de_perdas1(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1);
	}
	else{
		a = calculador_de_perdas1(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1);
		b = calculador_de_perdas1(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1);
	}
	DTABLE[numero_de_trocas-numero_de_trocas_restantes_atual][offset_do_canteiro]=numero_perdas+(a>b?b:a);
	return DTABLE[numero_de_trocas-numero_de_trocas_restantes_atual][offset_do_canteiro];
	
}

void print_dtable(int k, int n){
	int i,j;
	
	for(i=0;i<k; i++){
		for (j=0;j<n+1;j++){
			printf("%02d ",DTABLE[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	return;
}

void print_canteiro(){
	int i=0;
	while(i<=numero_molhos_de_flores){
		printf("|%d->(%d)vezes|",canteiro_em_linha[i].tipo,canteiro_em_linha[i].numero_de_elementos);
		i++;
	} 
	printf("\n");
}
