#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*structs*/
typedef struct molho_de_flores
{
	int tipo;
	int numero_de_elementos;

} Molho_de_flores;


/*variáveis globais*/
int numero_de_trocas=0;
Molho_de_flores canteiro_em_linha[1500];
int numero_molhos_de_flores=0;

int DTABLE[300][1500];


/*declaracao de funcoes auxiliares*/
int calculador_de_perdas1(int tipo_de_pesticida_atual, int offset_do_canteiro, int numero_de_trocas_restantes_atual);
void print_canteiro();
void print_dtable();

int main(){
	
	char flor;
	
	while(scanf("%d ",&numero_de_trocas)==1){
		
		scanf("%c",&flor);

		canteiro_em_linha[numero_molhos_de_flores].numero_de_elementos=1;
		canteiro_em_linha[numero_molhos_de_flores].tipo= flor - '0'; /*atoi(&flor)){*/
		
		while(scanf("%c",&flor)==1){
			if(flor=='\n'){
				break;
			}
			if(canteiro_em_linha[numero_molhos_de_flores].tipo== flor - '0'){ 
				canteiro_em_linha[numero_molhos_de_flores].numero_de_elementos++;
			}
			else{
				canteiro_em_linha[++numero_molhos_de_flores].numero_de_elementos=1;
				canteiro_em_linha[numero_molhos_de_flores].tipo= flor - '0'; /*atoi(&flor)){*/
			}
		}
		
		memset(DTABLE, 0, sizeof(DTABLE) ); /* 300 * 1500 * sizeof(int));*/
		int a=calculador_de_perdas1(1,0,numero_de_trocas-1);
		/*printf("A = %d\n", a);
		print_dtable(numero_de_trocas,numero_molhos_de_flores);*/

		memset(DTABLE, 0, sizeof(DTABLE) ); /* 300 * 1500 * sizeof(int));*/
		int b=calculador_de_perdas1(0,0,numero_de_trocas-1);
		/*printf("B = %d\n", b);*/
		/*print_dtable(numero_de_trocas,numero_molhos_de_flores);*/
		printf("%d\n",a<b?a:b); 
		
		numero_de_trocas=0;
		numero_molhos_de_flores=0;
	}
	return 0;
}


/*funcao recursiva*/
int calculador_de_perdas1(int tipo_de_pesticida_atual, int offset_do_canteiro, int numero_de_trocas_restantes_atual){
	
	
	if (DTABLE[numero_de_trocas - numero_de_trocas_restantes_atual][offset_do_canteiro]>0){
		/*printf("C ");*/
		return DTABLE[numero_de_trocas - numero_de_trocas_restantes_atual][offset_do_canteiro];
	}
	
	/*variaveis*/
	int i;
	int numero_perdas=0;
	
	/*atualizar numero de perdas*/
	if(tipo_de_pesticida_atual!=canteiro_em_linha[offset_do_canteiro].tipo){
		numero_perdas=canteiro_em_linha[offset_do_canteiro].numero_de_elementos;
	}
	if (numero_de_trocas_restantes_atual> numero_molhos_de_flores-offset_do_canteiro){
		return numero_perdas;
	}
	
	/*casos base*/
	if (offset_do_canteiro==numero_molhos_de_flores){
		return numero_perdas;
	}
	else if (numero_de_trocas_restantes_atual==0){
		int contador=0;
		if(tipo_de_pesticida_atual == canteiro_em_linha[offset_do_canteiro].tipo){
			i=offset_do_canteiro+1;	
		}
		else{
			i=offset_do_canteiro+2;	
		}
		while(i<=numero_molhos_de_flores){ /*&& numero_perdas_atual<numero_de_perdas_minimo){*/
			contador+=canteiro_em_linha[i].numero_de_elementos;	
			i+=2;
		}
		return numero_perdas+contador;
	}
	
	int a,b;
	/*chamada recursiva com e sem troca*/
	if (tipo_de_pesticida_atual==1){
		a =	calculador_de_perdas1(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1);
		b = calculador_de_perdas1(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual);
	}
	else if (tipo_de_pesticida_atual==0){
		a = calculador_de_perdas1(0, offset_do_canteiro+1,numero_de_trocas_restantes_atual);
		b = calculador_de_perdas1(1, offset_do_canteiro+1,numero_de_trocas_restantes_atual-1);
	}
	DTABLE[numero_de_trocas-numero_de_trocas_restantes_atual][offset_do_canteiro]=numero_perdas+(a>b?b:a);
	return DTABLE[numero_de_trocas-numero_de_trocas_restantes_atual][offset_do_canteiro];
	
}

void print_dtable(int k, int n){
	int i,j;
	
	for(i=0;i<k; i++){
		for (j=0;j<n+1;j++){
			printf("%02d ",DTABLE[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	return;
}

void print_canteiro(){
	int i=0;
	while(i<=numero_molhos_de_flores){
		printf("|%d->(%d)vezes|",canteiro_em_linha[i].tipo,canteiro_em_linha[i].numero_de_elementos);
		i++;
	} 
	printf("\n");
}
