import random
if __name__ == '__main__':
    for n in range(10):
        seq = ""
        trocas = random.randint(10, 200)
        lenSeq = random.randint(trocas*5, trocas*20)
        i = 0
        while i < lenSeq+trocas:
            um = random.randint(1, 10)
            zero = random.randint(1, 10)
            order = random.random()
            if order < 0.5:
                seq += um*"1" + zero*"0"
            else:
                seq += zero*"0" + um*"1"
            i += um + zero
            
        print(trocas,seq)
            